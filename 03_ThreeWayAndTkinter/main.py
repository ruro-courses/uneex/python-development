import functools as ft
import tkinter as tk
import tkinter.messagebox

import numpy as np


def make_grid(
        frame, contents,
        row_weights=None, col_weights=None,
        row_kwargs=None, col_kwargs=None,
        grid_kwargs=None,
):
    contents = np.asarray(contents)
    assert contents.ndim == 2, "Expected contents to be a 2D array"
    n_rows, n_cols = contents.shape

    row_weights = [1] * n_rows if row_weights is None else row_weights
    col_weights = [1] * n_cols if col_weights is None else col_weights
    row_kwargs = {} if row_kwargs is None else row_kwargs
    col_kwargs = {} if col_kwargs is None else col_kwargs
    grid_kwargs = {} if grid_kwargs is None else grid_kwargs

    assert len(row_weights) == n_rows, "row_weights must match content"
    assert len(col_weights) == n_cols, "col_weights must match content"

    for (i, j), c in np.ndenumerate(contents):
        assert isinstance(c, tk.Widget)
        c.grid(row=i, column=j, sticky=tk.NSEW, **grid_kwargs)

    for i, w in enumerate(row_weights):
        frame.rowconfigure(i, weight=w, **row_kwargs)

    for j, w in enumerate(col_weights):
        frame.columnconfigure(j, weight=w, **col_kwargs)


class Config:
    color1 = '#2b597f'
    color1_act = '#1f77b4'
    color2 = '#9b5d2c'
    color2_act = '#ff7f0e'
    button_border = 4
    button_minsize = 64
    board_shape = (4, 4)


class GameMenu(tk.Frame):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.newButton = tk.Button(
            self, text="New",
            command=self.master.board.shuffle_board,
        )
        self.exitButton = tk.Button(
            self, text="Exit",
            command=self.quit,
        )

        make_grid(
            self, [[
                tk.Frame(self),
                self.newButton,
                tk.Frame(self),
                tk.Frame(self),
                self.exitButton,
                tk.Frame(self),
            ]]
        )


def idx_to_pos(idx, board_shape):
    return np.unravel_index(idx - 1, board_shape)


def pos_to_idx(pos, board_shape):
    return 1 + np.ravel_multi_index(pos, board_shape)


def get_permutation_parity(perm: np.ndarray):
    cmps = perm > perm[:, np.newaxis]
    cmps &= np.tri(*cmps.shape, -1, dtype=np.bool_)
    return cmps.sum() % 2


class GameButton(tk.Button):
    def __init__(self, master, pos, *args, **kwargs):
        assert isinstance(master, GameBoard), \
            "GameButtons must belong to a game board"

        kwargs.setdefault('borderwidth', Config.button_border)
        kwargs.setdefault('command', ft.partial(master.press_button, pos=pos))
        super().__init__(master, *args, **kwargs)
        self.pos = pos

    @property
    def pos(self):
        return idx_to_pos(self.idx, self.master.board_shape)

    @pos.setter
    def pos(self, v):
        self.idx = pos_to_idx(v, self.master.board_shape)

    @property
    def idx(self):
        return int(self['text'])

    @idx.setter
    def idx(self, v):
        self['text'] = v


class GameBoard(tk.LabelFrame):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        height, width = Config.board_shape
        self.board_shape = (height, width)
        self.board_size = height * width
        self.buttons = np.empty(self.board_shape, dtype=np.object_)

        for pos in np.ndindex(self.board_shape):
            self.buttons[pos] = GameButton(self, pos)

        make_grid(
            self, self.buttons,
            col_kwargs={'minsize': Config.button_minsize},
            row_kwargs={'minsize': Config.button_minsize},
        )
        self.update_board()

    def shuffle_board(self):
        board = np.random.permutation(self.board_size)
        hole = np.argmax(board)
        board[hole], board[-1] = board[-1], board[hole]
        if get_permutation_parity(board.flatten()):
            board[0], board[1] = board[1], board[0]
            assert not get_permutation_parity(board.flatten())
        board += 1

        board = board.reshape(self.board_shape)
        for pos, b in np.ndenumerate(self.buttons):
            b.idx = board[pos]
        self.update_board()

    def get_adjacent(self, pos):
        bi, bj = pos
        h, w = self.board_shape
        return [
            self.buttons[i, j]
            for i, j in [
                (bi - 1, bj),
                (bi + 1, bj),
                (bi, bj - 1),
                (bi, bj + 1),
            ]
            if 0 <= i < h and 0 <= j < h
        ]

    def update_board(self):
        for pos, b in np.ndenumerate(self.buttons):
            if np.sum(b.pos) % 2:
                b['background'] = b['activebackground'] = Config.color1
            else:
                b['background'] = b['activebackground'] = Config.color2

            if b.idx == self.board_size:
                b.grid_remove()
            else:
                b.grid()

        hole, = [
            pos
            for pos, b in np.ndenumerate(self.buttons)
            if b.idx == self.board_size
        ]
        for adj in self.get_adjacent(hole):
            if np.sum(adj.pos) % 2:
                adj['activebackground'] = Config.color1_act
            else:
                adj['activebackground'] = Config.color2_act

    def check_win(self):
        idxs = np.vectorize(GameButton.idx.fget)(self.buttons.flat)
        if np.all(np.diff(idxs) >= 0):
            tk.messagebox.showinfo(message="You win!")
            self.shuffle_board()

    def press_button(self, pos):
        b = self.buttons
        for hole in self.get_adjacent(pos):
            if hole.idx == self.board_size:
                b[pos].idx, hole.idx = hole.idx, b[pos].idx
                self.update_board()
                self.check_win()
                break


class Game(tk.Frame):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.board = GameBoard(self)
        self.menu = GameMenu(self)

        make_grid(
            self, [[self.menu], [self.board]],
            row_weights=[0, 1]
        )


if __name__ == '__main__':
    root = tk.Tk()
    root.tk.call('tk', 'scaling', 1.0)
    root.title("15 Puzzle")

    game = Game(root)
    make_grid(root, [[game]])
    root.update()
    root.minsize(root.winfo_width(), root.winfo_height())
    root.mainloop()
