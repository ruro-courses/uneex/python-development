import os
import tkinter as tk
from tkinter.messagebox import showinfo

# region Debug/verbose reporting setup
if os.environ.get('DEBUG', ''):
    d_print = print
else:
    def d_print(*a, **kw):
        pass


# endregion
# region Parse and execute "location" strings
def parse_tail_default(text, tail, default, value_conv=None):
    if tail in text:
        text, value = text.split(tail)
        if value_conv is not None:
            value = value_conv(value)
    else:
        value = default
    return text, value


def parse_span_weight(desc):
    desc, span = parse_tail_default(desc, '+', 0, int)
    desc, weight = parse_tail_default(desc, '.', 1, int)
    return int(desc), span, weight


def parse_location(loc):
    loc, grav = parse_tail_default(loc, '/', tk.NSEW.upper())
    row, col = loc.split(':')
    return parse_span_weight(row), parse_span_weight(col), grav


def set_location(w, loc):
    (row, row_s, row_w), (col, col_s, col_w), grav = parse_location(loc)
    d_print(
        f"{loc=:<12}:"
        f"({row=}.{row_s=}+{row_w=}):"
        f"({col=}.{col_s=}+{col_w=})"
        f"/{grav=}"
    )
    w.grid(
        row=row,
        column=col,
        sticky=grav,
        rowspan=1 + row_s,
        columnspan=1 + col_s
    )
    w.master.rowconfigure(row, weight=row_w)
    w.master.columnconfigure(col, weight=col_w)


# endregion
# region Dynamic Widget implementation
def make_dynamic_widget(name, kind_, init_kwargs=(), init_hook=None):
    kind = kind_.__name__
    d_print(f"Generating dynamic widget of {name=} {kind=}")

    class DynWidget(kind_):
        def __init__(self, **kwargs):
            d_print(f"Initializing dynamic widget of {name=} {kind=}")
            self._fields = set()

            captured_kwargs = {k: kwargs.pop(k, None) for k in init_kwargs}
            super().__init__(**kwargs)
            if init_hook is not None:
                init_hook(self, **captured_kwargs)

        def __repr__(self):
            return "Dynamic" + super().__repr__() + f"[{name=}]"

        def __str__(self):
            return "Dynamic" + super().__str__() + f"[{name=}]"

        def __getattr__(self, sub_name):
            if sub_name in self._fields:
                raise AttributeError(
                    "Attempting to fetch an uninitialized field "
                    f"{sub_name=} in dynamic widget of {name=} {kind=}"
                )
            self._fields.add(sub_name)

            d_print(f"Creating callback for {sub_name=} of {name=} {kind=}")

            def callback(sub_kind_, sub_location, **sub_kwargs):
                sub_kind = sub_kind_.__name__
                d_print(
                    f"Executing callback for {sub_name=} of {name=} {kind=} "
                    f"with {sub_kind=} {sub_location=} {sub_kwargs=}"
                )
                dyn_widget = make_dynamic_widget(sub_name, sub_kind_)
                sub_widget = dyn_widget(master=self, **sub_kwargs)
                setattr(self, sub_name, sub_widget)
                d_print(f"Setting location for {sub_name=}, {sub_location=}")
                set_location(sub_widget, sub_location)

            return callback

    return DynWidget


def application_init(self, title):
    master = self.master
    master.title(title)
    master.tk.call('tk', 'scaling', 1.0)
    self.createWidgets()
    set_location(self, '0:0')
    master.update()
    master.minsize(master.winfo_width(), master.winfo_height())


Application = make_dynamic_widget(
    'Application', tk.Frame, init_kwargs=('title',), init_hook=application_init
)


# endregion
# region Test App

# FixMe: please format your "testing" code appropriately in the future, so that
#        we don't have to surround it with tons of "noinspection/noqa" comments

# @formatter:off
# noinspection PyPep8Naming,PyAttributeOutsideInit
class App(Application):
    def createWidgets(self):
        self.message = "Congratulations!\nYou've found a sercet level!"
        self.F1(tk.LabelFrame, "1:0", text="Frame 1")
        self.F1.B1(tk.Button, "0:0/NW", text="1")
        self.F1.B2(tk.Button, "0:1/NE", text="2")
        self.F1.B3(tk.Button, "1:0+1/SEW", text="3")
        self.F2(tk.LabelFrame, "1:1", text="Frame 2")
        self.F2.B1(tk.Button, "0:0/N", text="4")
        self.F2.B2(tk.Button, "0+1:1/SEN", text="5")
        self.F2.B3(tk.Button, "1:0/S", text="6")
        self.Q(tk.Button, "2.0:1.2/SE", text="Quit", command=self.quit)
        self.F1.B3.bind("<Any-Key>", lambda event: showinfo(self.message.split()[0], self.message)) # noqa

app = App(title="Sample application") # noqa
app.mainloop()
# @formatter:on
# endregion
