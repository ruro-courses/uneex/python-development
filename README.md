# [UNИX Collaborative Python Development 2021]

**Important!** Images and other binary files are stored in
[`git lfs`](https://git-lfs.github.com/).
If you clone this repository and the contents of the image files look like this
```
version https://git-lfs.github.com/spec/v1
oid sha256:c097bb054ab7e0d92c383ae94c86a2436971adda8041139ff5a91314046fbb73
size 522799
```
then you haven't installed or enabled `git lfs` correctly. You need to install
`git lfs` from your packege manager and then run `git lfs install`.

03. [Пятнашки](./03_ThreeWayAndTkinter/)
04. [Задача-головоломка](./04_PublicRepositoryEvents/)
05. [Вырожденный графический редактор](./05_SshAndSmartWidgents/)
